from django.urls import path
from . import views

urlpatterns = [
    path('', views.getSandboxPage),
    path('neumorphicButton', views.getNeumorphicButtonPage),
    path('vantaNet', views.getVantaNetPage),
]
