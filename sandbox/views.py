from django.shortcuts import render

# Create your views here.
def getSandboxPage(request):
    return render(request,'sandbox/sandboxPage.html')

def getNeumorphicButtonPage(request):
    return render(request,'sandbox/neumorphicButtonPage.html')

def getVantaNetPage(request):
    return render(request,'sandbox/vantaNetPage.html')