from django.shortcuts import redirect, render

def getLandingPage(request):
    return render(request, 'landingPage.html')