VANTA.NET({
    el: ".net",
    mouseControls: true,
    touchControls: true,
    minHeight: 200.00,
    minWidth: 200.00,
    scale: 1.00,
    scaleMobile: 1.00,
    backgroundColor: 0xf9f9f9,
    points: 10.00,
    maxDistance: 10.00,
    spacing: 25.00
});