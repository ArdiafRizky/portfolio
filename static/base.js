gototop = document.querySelector('.gototop');
projectDiv = document.querySelector('.neumos');
header = document.querySelector('.net');

window.onscroll = function(){
    hideGototop()
};

// let x = document.querySelector('#contact');
// let trans = document.querySelector('.transition');
// x.addEventListener('click', function(){
//     trans.classList.add('anim-trans');
//     setTimeout(function(){trans.classList.remove('anim-trans');},3000);
// });

function redirectTo(x){
    setTimeout(function(){window.location = x;},1500);
}

function hideGototop(){
    if (window.pageYOffset > 0.75*header.offsetHeight){
        gototop.classList.add('block');
    } else {
        gototop.classList.remove('block');
    }
}

function getToTop(){
    header.scrollIntoView({behavior:'smooth'});
}

function scrollToThe(x){
    target = document.querySelector(x);
    target.scrollIntoView({behavior:'smooth'});
}