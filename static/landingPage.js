// const ScrollReveal = require('scrollreveal')

let neumos = document.querySelector('.neumos');
let allNeumoProject = document.querySelectorAll('.neumo-project');
let displayImg = document.querySelector('#display-img');
let btnProject = document.querySelector('#btn-project');
let btnAbout = document.querySelector('.line');
let btnContact = document.querySelector('#btn-contact');

const projectImg = {
    'Guru-guru':'guruguru.png',
    'BeraniSehat':'beranisehat.png',
    'Medikago':'medikago.png',
    'PPW Stories': 'ppw.png',
    'Element Sandbox': 'sandbox.png',
}

const myTags = [
    'JavaScript', 'HTML5', 'CSS', 'Python', 'Java', 'Dart', 'Flutter',
    'Django', 'Express', 'Node.js', 'React.js', 'Bootstrap4', 'PostgreSQL',
    'C', 'Linux', 'JSON', 'RestAPI', 'Materialize', 'XML',
]

// ScrollReveal().reveal('.about')

let tagCloud = TagCloud('.cloud-tag', myTags, {
        radius: 180,
        maxSpeed: 'normal',
        initSpeed: 'slow',
        // 0 = top
        // 90 = left
        // 135 = right-bottom
        direction: 135,
        // interact with cursor move on mouse out
        keep: true
});

for(let i = 0; i < allNeumoProject.length; i++){
    allNeumoProject[i].addEventListener('mouseover', function(){
        pname = this.querySelector('p').innerHTML;
        displayImg.setAttribute('src', "/static/assets/"+projectImg[pname]);
    });
}

function initMap(){
    var jakut = {lat: -6.113741, lng: 106.921713};
    var map = new google.maps.Map(document.querySelector('.contact-right-map'), {zoom: 4, center: jakut});
    var marker = new google.maps.Marker({position: jakut, map: map});
}

function redirectTo(x){
    // setTimeout(function(){window.location = x;},1500);
    window.open(x);
}

function scrollToThe(x){
    console.log('sc');
    target = document.querySelector(x);
    target.scrollIntoView({behavior:'smooth'});
}